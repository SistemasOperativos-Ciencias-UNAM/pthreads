/******************************************************************************
* FILE: hello_ret.c
* DESCRIPTION:
*   A "hello world" Pthreads program which demonstrates one safe way
*   to return values from threads during exit.
* AUTHOR: Blaise Barney
* REVISION: 08/04/15
* LAST REVISED: 15/01/21  Andres Hernandez
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define NUM_THREADS 8
#define BUF_SIZE 64

char *messages[NUM_THREADS];
char return_values[NUM_THREADS][BUF_SIZE];

void *PrintHello(void *threadid);

int main(int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    long taskids[NUM_THREADS];
    int rc, t;

    messages[0] = "English: Hello World!";
    messages[1] = "French: Bonjour, le monde!";
    messages[2] = "Spanish: Hola al mundo";
    messages[3] = "Klingon: Nuq neH!";
    messages[4] = "German: Guten Tag, Welt!";
    messages[5] = "Russian: Zdravstvuyte, mir!";
    messages[6] = "Japan: Sekai e konnichiwa!";
    messages[7] = "Latin: Orbis, te saluto!";

    for(t=0; t<NUM_THREADS; t++)
    {
        taskids[t] = t;
        bzero(return_values[t], BUF_SIZE);
        printf("Creating thread %d\n", t);
        rc = pthread_create(&threads[t], NULL, PrintHello, (void *) taskids[t]);
        if (rc)
        {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }

    for(t=0; t < NUM_THREADS; t++)
    {
        pthread_join(threads[t], NULL);
    }

    for(t=0; t < NUM_THREADS; t++)
    {
        printf("(%d):\t%s\n", t, return_values[t]);
    }

    pthread_exit(NULL);
}

void *PrintHello(void *threadid)
{
    long taskid;
    char* p = 0;
    int pos = 0;
#ifdef SLEEP
    sleep(1);
#endif
    taskid = (long) threadid;
    printf("Thread %ld: %s\n", taskid, messages[taskid]);
    p = strchr(messages[taskid], ':');
    pos = p - messages[taskid] ;
    strncpy(return_values[taskid], messages[taskid], pos);
    pthread_exit(NULL);
}

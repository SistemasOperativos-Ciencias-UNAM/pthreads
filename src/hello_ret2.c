/******************************************************************************
* FILE: hello_arg2.c
* DESCRIPTION:
*   A "hello world" Pthreads program which demonstrates another safe way
*   to return values from threads during exit.  In this case,
*   a structure is used to pass and return multiple arguments.
* AUTHOR: Blaise Barney
* REVISION: 01/29/09
* LAST REVISED: 15/01/21  Andres Hernandez
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define NUM_THREADS 8
#define BUF_SIZE 64

struct thread_data
{
    int	thread_id;
    int  sum;
    char *message;
    char return_value[BUF_SIZE];
};

struct thread_data thread_data_array[NUM_THREADS];
char *messages[NUM_THREADS];

void *PrintHello(void *threadarg);

int main(int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    int *taskids[NUM_THREADS];
    int rc, t, sum;

    sum=0;
    messages[0] = "English: Hello World!";
    messages[1] = "French: Bonjour, le monde!";
    messages[2] = "Spanish: Hola al mundo";
    messages[3] = "Klingon: Nuq neH!";
    messages[4] = "German: Guten Tag, Welt!";
    messages[5] = "Russian: Zdravstvytye, mir!";
    messages[6] = "Japan: Sekai e konnichiwa!";
    messages[7] = "Latin: Orbis, te saluto!";

    for(t=0; t<NUM_THREADS; t++)
    {
        sum = sum + t;
        thread_data_array[t].thread_id = t;
        thread_data_array[t].sum = sum;
        thread_data_array[t].message = messages[t];
        bzero(thread_data_array[t].return_value, BUF_SIZE);

        printf("Creating thread %d\n", t);
        rc = pthread_create(&threads[t], NULL, PrintHello, (void *) &thread_data_array[t]);
        if (rc)
        {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }

    for(t=0; t < NUM_THREADS; t++)
    {
        pthread_join(threads[t], NULL);
    }

    for(t=0; t < NUM_THREADS; t++)
    {
        printf("(%d):\t%s\n", t, thread_data_array[t].return_value);
    }

    pthread_exit(NULL);
}

void *PrintHello(void *threadarg)
{
    int taskid, sum;
    struct thread_data *my_data;
    char *hello_msg, *return_value;
    char* p = 0;
    int pos = 0;
#ifdef SLEEP
    sleep(1);
#endif
    my_data = (struct thread_data *) threadarg;
    taskid = my_data->thread_id;
    sum = my_data->sum;
    hello_msg = my_data->message;
    return_value = my_data->return_value;
    printf("Thread %d: %s  Sum=%d\n", taskid, hello_msg, sum);
    p = strchr(hello_msg, ':');
    pos = p - hello_msg;
    strncpy(return_value, hello_msg, pos);
    pthread_exit(NULL);
}

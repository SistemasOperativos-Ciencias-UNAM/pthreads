/******************************************************************************
* FILE: bug5.c
* DESCRIPTION:
*   A simple pthreads program that dies before the threads can do their
*   work. Figure out why.
* AUTHOR: 07/06/05 Blaise Barney
* REVISION: 07/11/12
* LAST REVISED: 23/08/17  Andres Hernandez
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>

#define NUM_THREADS 5

void *PrintHello(void *threadid);

int main(int argc, char *argv[])
{
    int rc;
    long t;
    pthread_t threads[NUM_THREADS];
    for(t=0; t<NUM_THREADS; t++)
    {
        printf("Main: creating thread %ld\n", t);
        rc = pthread_create(&threads[t], NULL, PrintHello, (void *)t);
        if (rc)
        {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }
    printf("Main: Done.\n");
}

void *PrintHello(void *threadid)
{
    int i;
    double myresult=0.0;
    printf("thread=%ld: starting...\n", (long) threadid);
    for (i=0; i<1000000; i++)
        myresult += sin(i) * tan(i);
    printf("thread=%ld result=%e. Done.\n", (long) threadid, myresult);
    pthread_exit(NULL);
}

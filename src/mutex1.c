/* http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define N_THREADS 2

int  counter = 0;
void *thread_function(void *arg);
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int main(void)
{
    int i = 0;
    int rc[N_THREADS];
    pthread_t threads[N_THREADS];

    /* Create independent threads each of which will execute thread_function */
    for (i=0; i<N_THREADS; i++)
    {
        rc[i] = pthread_create(&threads[i], NULL, &thread_function, (void *) (long)i);
        if (rc[i] != 0)
        {
            printf("Thread creation failed: %d\n", rc[i]);
        }
    }

    /* Wait till threads are complete before main continues.
       Unless we wait we run the risk of executing an exit which will terminate
       the process and all threads before the threads have completed. */

    for (i=0; i<N_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("FINAL Counter value: %d\n", counter);
    exit(EXIT_SUCCESS);
}

/* Thread work function */
void *thread_function(void *arg)
{
    long i = 0 ;
    long my_arg = (long) arg;
    printf("Thread number %ld (%p)\n", my_arg, (void *) pthread_self());
    /* Perform operation using mutex */
    pthread_mutex_lock(&mutex);
    for (i = 0 ; i<10 ; i++)
    {
        counter++;
        printf("Counter value: %d\n", counter);
    }
    pthread_mutex_unlock(&mutex);
    pthread_exit(NULL);
}
